//
//  NoGitExampleTests.m
//  NoGitExampleTests
//
//  Created by Muhammad Umair Ayoub on 27/07/2016.
//  Copyright (c) 2016 Muhammad Umair Ayoub. All rights reserved.
//

#import "NoGitExampleTests.h"

@implementation NoGitExampleTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in NoGitExampleTests");
}

@end
