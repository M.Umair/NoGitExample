//
//  AppDelegate.h
//  NoGitExample
//
//  Created by Muhammad Umair Ayoub on 27/07/2016.
//  Copyright (c) 2016 Muhammad Umair Ayoub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
