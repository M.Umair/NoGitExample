//
//  ViewController.m
//  NoGitExample
//
//  Created by Muhammad Umair Ayoub on 27/07/2016.
//  Copyright (c) 2016 Muhammad Umair Ayoub. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
