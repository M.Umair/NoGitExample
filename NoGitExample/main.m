//
//  main.m
//  NoGitExample
//
//  Created by Muhammad Umair Ayoub on 27/07/2016.
//  Copyright (c) 2016 Muhammad Umair Ayoub. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
